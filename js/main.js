
// window.onscroll = function() {myFunction()};

// var navbar = document.getElementById("sticky");
// var sticky = navbar.offsetTop;

// function myFunction() {
//   if (window.pageYOffset >= sticky) {
//     navbar.classList.add("sticky")
//   } else {
//     navbar.classList.remove("sticky");
//   }
// }

// function myBar(x) {
//     x.classList.toggle("change");
//   }
//   function openNav() {
//     document.getElementById("mySidenav").style.height = "100%";
//   }
  
//   function closeNav() {
//     document.getElementById("mySidenav").style.height = "0";
//   }

 window.onscroll = function() {myFunction()};

 var navbar = document.getElementById("sticky");
 var sticky = navbar.offsetTop;

 function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
}

 function myBar(x) {
    x.classList.toggle("change");
  }
  function openNav() {
    document.getElementById("mySidenav").style.height = "100%";
  }
  
  function closeNav() {
    document.getElementById("mySidenav").style.height = "0";
  }
  function onReady(callback) {
    var intervalId = window.setInterval(function() {
      if (document.getElementsByTagName('body')[0] !== undefined) {
        window.clearInterval(intervalId);
        callback.call(this);
      }
    }, 3000);
  }
  
  function setVisible(selector, visible) {
    document.querySelector(selector).style.display = visible ? 'block' : 'none';
  }
  
  onReady(function() {
    setVisible('body', true);
    setVisible('#loading', false);
  });

  var swiper = new Swiper('.swiper-container', {
    spaceBetween: 30,
    loop: true,
    loopedSlides: 1,
    centeredSlides: true,
    autoplay: {
      delay: 6000,
      disableOnInteraction: false,
    },
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });

