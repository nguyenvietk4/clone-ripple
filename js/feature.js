// $(document).ready(function(){
//     $('.single-item').slick({

//     });
//   });
// $('.fade').slick({
//     dots: true,
//     infinite: true,
//     speed: 500,
//     fade: true,
//     cssEase: 'linear'
//   });

// $('.one-time').slick({
//     dots: true,
//     infinite: true,
//     speed: 300,
//     slidesToShow: 1,
//     arrows:false,

//   });

var mySwiper = new Swiper('.swiper-container', {
  

  // If we need pagination
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },

  // Navigation arrows
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },


})

$(document).ready(function () {

  var quoteSwiper = new Swiper('.quote-slider');

  var imageSwiper = new Swiper('.image-slider', {
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
  });

  quoteSwiper.params.control = imageSwiper;
  imageSwiper.params.control = quoteSwiper;
});